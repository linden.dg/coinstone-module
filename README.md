## Tasks
The gulpfile provides the following tasks:


- `build`: Cleans dist and builds TypeScript, Less or Sass, and copies fonts, templates, and JSON to dist.

- `link`: Creates a symbolic link to dist in Foundry's User Data folder (use --clean to remove the build from User Data).

- `watch`: Watch for changes in src and run the build task.

- `clean`: Quickly clean dist, removing the build files.

- `package`: Quickly packages the build for testing or manual distribution.

- `publish -u <version | major | minor | patch>`: Updates the manifest (version and URLs), and handles Git operations to publish a new version (version format is major.minor.patch.